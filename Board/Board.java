package Board;

import Player.Player;
import Board.Field.CircleField;
import Board.Field.CrossField;
import Board.Field.EmptyField;
import Board.Field.Field;
import GameInterface.InfoBox;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class Board {

    private Field[][] board = new Field[3][3];
    private Player player;
    private GridPane grid = new GridPane();
    private Pane layout;
    private boolean game = true;

    public Board(Player player) {

        grid.setPadding(new Insets(150, 0, 0, 60));
        this.player = player;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++) {
                board[i][j] = new EmptyField();
                int tempI = i;
                int tempJ = j;
                board[i][j].getImageView().setOnMouseClicked(e -> updateBoard(tempI, tempJ));
            }
    }

    public void drawBoard(Pane layout) {

        this.layout = layout;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                grid.add(board[i][j].draw(), i, j);
        layout.getChildren().add(grid);
    }

    public void updateScreen() {

        Image infoImage;
        ImageView infoView = new ImageView();
        infoView.setFitWidth(70);
        infoView.setFitHeight(70);

        if(player.isCircleTurn())
            infoImage = new Image("img/smallcircle.png");
        else
            infoImage = new Image("img/smallcross.png");

        infoView.setImage(infoImage);
        infoView.setLayoutX(890);
        infoView.setLayoutY(330);
        layout.getChildren().add(infoView);

        GridPane grid2 = new GridPane();
        grid2.setPadding(new Insets(150, 0, 0, 60));
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                grid2.add(board[i][j].draw(), i, j);
        layout.getChildren().add(grid2);
    }

    public void updateBoard(int x, int y) {

        if(game) {
            if (this.board[x][y].isEmpty()) {
                if (player.isCircleTurn()) {
                    this.board[x][y] = new CircleField();
                    player.crossTurn();
                } else if (!player.isCircleTurn()) {
                    this.board[x][y] = new CrossField();
                    this.player.circleTurn();
                }
                this.board[x][y].setNotEmpty();
                player.setCounter();
                updateScreen();
                checkVertical(x);
                checkHorizontal(y);
                checkDiagonal();
                if (player.getCounter() == 9)
                    InfoBox.display("Remis");
            }
        }
    }

    public void checkVertical(int x) {

        int circle = 0;
        int cross = 0;

        for(int i=0; i<3; i++) {
            if(this.board[x][i].getClass() == CircleField.class) {
                if(++circle == 3) {
                    InfoBox.display("Wygrywa kółko");
                    this.game = false;
                }
            }
            else if(this.board[x][i].getClass() == CrossField.class) {
                if (++cross == 3) {
                    InfoBox.display("Wygrywa krzyżyk");
                    this.game = false;
                }
            }
        }
    }

    public void checkHorizontal(int y) {

        int circle = 0;
        int cross = 0;

        for(int i=0; i<3; i++) {
            if(this.board[i][y].getClass() == CircleField.class) {
                if(++circle == 3) {
                    InfoBox.display("Wygrywa kółko");
                    this.game = false;
                }
            }
            else if(this.board[i][y].getClass() == CrossField.class) {
                if (++cross == 3) {
                    InfoBox.display("Wygrywa krzyżyk");
                    this.game = false;
                }
            }
        }
    }

    public void checkDiagonal() {

        int circle = 0;
        int cross = 0;

        for(int i=0; i<3; i++) {
            if (this.board[i][i].getClass() == CircleField.class) {
                if (++circle == 3) {
                    InfoBox.display("Wygrywa kółko");
                    this.game = false;
                }
            } else if (this.board[i][i].getClass() == CrossField.class) {
                if (++cross == 3) {
                    InfoBox.display("Wygrywa krzyżyk");
                    this.game = false;
                }
            }
        }
        if((this.board[0][2].getClass() == CircleField.class)
                && (this.board[1][1].getClass() == CircleField.class)
                && (this.board[2][0].getClass() == CircleField.class)) {
            InfoBox.display("Wygrywa kółko");
            this.game = false;
        }
        else if((this.board[0][2].getClass() == CrossField.class)
                && (this.board[1][1].getClass() == CrossField.class)
                && (this.board[2][0].getClass() == CrossField.class)) {
            InfoBox.display("Wygrywa krzyżyk");
            this.game = false;
        }
    }
}
package Board.Field;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class Field implements Drawable {

    protected Image image;
    protected ImageView imageView;
    protected boolean empty = true;

    public boolean isEmpty() { return empty; }

    public void setNotEmpty() {
        empty = false;
    }

    public ImageView getImageView() {
        return this.imageView;
    }

}

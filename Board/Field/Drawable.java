package Board.Field;

import javafx.scene.image.ImageView;

public interface Drawable {

    ImageView draw();
}

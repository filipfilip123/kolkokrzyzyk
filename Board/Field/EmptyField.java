package Board.Field;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class EmptyField extends Field {

    public EmptyField() {
        image = new Image("img/EmptyField.png");
        imageView = new ImageView();
    }

    @Override
    public ImageView draw() {

        imageView.setFitHeight(140);
        imageView.setFitWidth(140);
        imageView.setImage(image);

        return imageView;
    }
}

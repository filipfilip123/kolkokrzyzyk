package Player;

public class Player {

    private boolean circleTurn = true;
    private int counter = 0;

    public void circleTurn() {
        this.circleTurn = true;
    }

    public void crossTurn() {
        this.circleTurn = false;
    }

    public boolean isCircleTurn() {
        return this.circleTurn;
    }

    public void setCounter(){ this.counter++; }

    public int getCounter() {
        return this.counter;
    }
}
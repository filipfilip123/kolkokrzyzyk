package Board.Field;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class CrossField extends Field {

    public CrossField() {
        image = new Image("img/Cross.png");
        imageView = new ImageView();
    }

    @Override
    public ImageView draw() {

        imageView.setFitHeight(140);
        imageView.setFitWidth(140);
        imageView.setImage(image);

        return imageView;
    }
}

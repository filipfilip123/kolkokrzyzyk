import Board.Board;
import GameInterface.Display;
import Player.Player;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public void start(Stage primaryStage) {

        Player player = new Player();
        Board board = new Board(player);
        Display display = new Display(player, board, primaryStage);
        display.display(primaryStage, board);
    }

    public static void main(String[] args) {
        launch(args);
    }

}

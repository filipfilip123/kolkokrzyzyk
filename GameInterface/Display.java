package GameInterface;

import Board.Board;
import Player.Player;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Display {

    private Player player;
    private Board board;
    private Stage primaryStage;

    public Display(Player player, Board board, Stage primaryStage) {
        this.player = player;
        this.board = board;
        this.primaryStage = primaryStage;
    }

    public void resetGame() {

        this.player = new Player();
        this.board = new Board(player);
        display(primaryStage, board);
    }

    public void display(Stage stage, Board board) {

        stage.setTitle("Kółko i krzyżyk");
        stage.setResizable(false);

        Pane layout = new Pane();

        ImageView iv = new ImageView();
        Image img = new Image("img/smallcircle.png");
        iv.setImage(img);
        iv.setLayoutX(890);
        iv.setLayoutY(330);
        layout.getChildren().add(iv);

        Button button = new Button("Od nowa");
        button.setFont(new Font(24));
        button.setLayoutX(735);
        button.setLayoutY(460);
        button.setPrefSize(150,50);
        button.setOnAction(e->resetGame());
        layout.getChildren().add(button);

        board.drawBoard(layout);

        Label info = new Label("Tura gracza:");
        info.setFont(new Font(32));
        info.setLayoutX(700);
        info.setLayoutY(340);
        layout.getChildren().add(info);

        stage.setScene(new Scene(layout,1080, 720));
        stage.show();
    }
}

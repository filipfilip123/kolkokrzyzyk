package GameInterface;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class InfoBox {

    public static void display(String message) {

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Wygrana");
        window.setResizable(false);

        Label info = new Label();
        info.setText(message);

        Button exitButton = new Button("OK");
        exitButton.setOnAction(e->window.close());

        VBox layout = new VBox(10);
        layout.getChildren().addAll(info, exitButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout, 200, 100);
        window.setScene(scene);
        window.showAndWait();
    }
}
